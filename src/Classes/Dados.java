package Classes;

public class Dados {
    //este codigo é para os usuarios
    
   private int maxusuarios = 10; 
   private Usuario Musuarios[]= new Usuario[maxusuarios];
   
   private int contusuarios =0;
 
   //este codigo é para os produtos
   
   private int maxprod = 20; 
   
   private Produto Mprodutos[] = new Produto[maxprod];
   private int countprodutos =0;
   
   
   //este é o codigo para os clientes
   
   private int maxCli = 10;
   private Cliente mClientes[]= new Cliente[maxCli];
   private int countclientes = 0;
   
   //cadastro de usuarios
   
   public Dados() {
       Usuario Musuario = new Usuario("Miguel","Miguel","Maciel","123");
       Musuarios[contusuarios]=Musuario;
       contusuarios ++;
       
       Musuario = new Usuario("Anthonia","Anthonia","Soares","123");
       Musuarios[contusuarios]=Musuario;
       contusuarios ++;
       
       Musuario = new Usuario("joão","joão","Maria","123");
       Musuarios[contusuarios]=Musuario;
       contusuarios ++;

       Musuario = new Usuario("Maria","Maria","joão","123");
       Musuarios[contusuarios]=Musuario;
       contusuarios ++;

       Musuario = new Usuario("2006","Pedro","Soares","123");
       Musuarios[contusuarios]=Musuario;
       contusuarios ++;

       Musuario = new Usuario("J356","Jorge","Rodrigues","123");
       Musuarios[contusuarios]=Musuario;
       contusuarios ++;

       Musuario = new Usuario("Lucifer","Tom Ellis","Netflix","123");
       Musuarios[contusuarios]=Musuario;
       contusuarios ++;

       Musuario = new Usuario("Rosa","Rosa","Neves","123");
       Musuarios[contusuarios]=Musuario;
       contusuarios ++;

       Musuario = new Usuario("Rosa","Rosa","Flores","123");
       Musuarios[contusuarios]=Musuario;
       contusuarios ++;

       Musuario = new Usuario("Beatriz","Beatriz","Gomes","123");
       Musuarios[contusuarios]=Musuario;
       contusuarios ++;

       
    //cadastros de produtos
   Produto Mproduto;
   
       // QUANDO EU HABILITO O CAMPO ABAIXO PEDI O CONSTRUTOR E EU NÃO SEI COMO RESOLVER!!!!
       
   Mproduto = new Produto("1","Hamburguer","","aa",25,"Especial Com Chedar");
   Mprodutos[countprodutos]=Mproduto;
    countprodutos ++;
   
   }
   //cadastro de clientes
   Cliente mClientes;
   
   
    mClientes = new Cliente("1","Rafael","Madeira","Montenegro 761","984541445",Utilidades.StringToDate(2019/12/4);
    mClientes[countclientes]=mClientes;
    countclientes ++;
 
   

      //USUARIOS
   public int NUsuarios(){
       return contusuarios;
   }
    //PRODUTOS
   public int NUprodutos(){
       return countprodutos;
   }
   
   //CLIENTES
   public int NUClientes(){
       return countclientes;
   }
   
   
   
      //USUARIOS
   public Usuario[] getUsuarios(){
       return Musuarios;
   }
   
   //PRODUTOS
   public Produto[] getprodutos(){
       return Mprodutos;
   }
   
    //CLIENTES
   public Cliente[] getClientes(){
       return mClientes;
   }  
   
   //VALIDAR USUARIOS
    public int LinhaUsuario(String usuario){
     
        for (int i = 0; i <contusuarios; i++) {
            if(Musuarios[i].getCodusuario().equals(usuario)){
            return i;
        }
            
        }
        return -1;
    }
     //VALIDAR PRODUTOS
    public int LinhaProduto(String produto){
     
        for (int i = 0; i <countprodutos; i++) {
            if(Mprodutos[i].getCodproduto().equals(produto)){
            return i;
        }
            
        }
        return -1;
    }
    //VALIDAR CLIENTES
     public int LinhaCliente(String cliente){
     
        for (int i = 0; i <countclientes; i++) {
            if(mClientes[i].getCodcliente().equals(cliente)){
            return i;
        }
            
        }
        return -1;
    }
        //CADASTRAR USUARIOS
    public String CadUsuario(Usuario MMusuario){
        if (contusuarios == maxusuarios) {
            return"você não mais espaço para novos cadastros!!!";
            
        }
        Musuarios[contusuarios]= MMusuario;
        contusuarios++;
        return "usuario Cadastrado Com Sucesso!!";
    }
    //CADASTRAR PRODUTOS
    public String CodProduto(Produto MMprodutos){
        if (countprodutos == maxprod) {
            return"você não mais espaço para novos cadastros!!!";
            
        }
        
        Mprodutos[countprodutos] = MMprodutos;
        countprodutos++;
        return "Produto Cadastrado Com Sucesso!!";
    }
    //CADASTRAR CLIENTES
    
    public String CadCliente(Cliente mCliente){
        if (countclientes == maxCli) {
            return"você não tem mais espaço!!";
            
        }
        mClientes[countclientes]= mCliente;
        countclientes++;
        return "Cliente Cadastrado Com Sucesso!!";
    }
    //EDITAR USUARIOS
    public String EditarUsuario(Usuario MMusuario, int poslinha){
        
        Musuarios[poslinha].setNome(MMusuario.getNome());
        Musuarios[poslinha].setNome(MMusuario.getSobrenome());
        Musuarios[poslinha].setNome(MMusuario.getSenha());
      
            return "usuario Editado Com Sucesso!!";
}
    //EDITAR PRODUTOS
    public String EditarProduto(Produto Mproduto, int poslinha){
        
        Mprodutos[poslinha].setDescricao(Mproduto.getDescricao());
        Mprodutos[poslinha].setPreco(Mproduto.getPreco());
        Mprodutos[poslinha].setObs(Mproduto.getObs());
       
        
      
            return "Produto Editado Com Sucesso!!";
}
    //EDITAR CLIENTES
    
     public String EditarCliente(Cliente mCliente, int poslinha){
        mClientes[poslinha].setEndereco(mCliente.getEndereco());
        mClientes[poslinha].setNome(mCliente.getNome());
        mClientes[poslinha].setTelefone(mCliente.getTelefone());
        mClientes[poslinha].setCaldata(mCliente.getCaldata());
        
      
            return "cliente editado Com Sucesso!!";
}
    //DELETAR USUARIOS
     
    public String DeletarUsuario(Usuario MMusuario, int poslinha){
        for (int i = 0; i <contusuarios; i++) {
            Musuarios[i]=Musuarios[i + 1];
            
            
        }
        
        contusuarios --;
         return "usuario deletado Com Sucesso!!";
       
      
           
}
    //DELETAR PRODUTOS
    
     public String DeletarProduto( int poslinha){
        for (int i = 0; i <countprodutos; i++) {
            Mprodutos[i]=Mprodutos[i + 1];
            
            
        }
        
        countprodutos --;
         return "Produto deletado Com Sucesso!!";
       
    
     }
     //DELETAR CLIENTES
    
     public String DeletarClientes( int poslinha){
        for (int i = 0; i <countclientes; i++) {
            mClientes[i]=mClientes[i + 1];
            
            
        }
        
        countprodutos --;
         return "cliente deletado Com Sucesso!!";
       
    
     }
     //VALIDAR USUARIOS
    
    public boolean ValidarUsuario (String usuario, String senha){
        boolean x= false;
        for (int i = 0; i < contusuarios; i++) {
            if(Musuarios[i].getCodusuario().equals(usuario)&&Musuarios[i].getSenha().equals(senha)){
                return true;
            }
        }
        return false;
    }
    
    
 
}