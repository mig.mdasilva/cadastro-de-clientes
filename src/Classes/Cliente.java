package Classes;

import java.util.Date;

public class Cliente extends Usuario {
   
        private String codcliente;
        //private String nome;
        private String endereco;
        private String telefone;
        private Date caldata;
        //private String sobrenome;

    public Cliente(String codcliente, String endereco, String telefone, Date caldata, String codusuario, String nome, String sobrenome, String senha) {
        super(codusuario, nome, sobrenome, senha);
        this.codcliente = codcliente;
        this.endereco = endereco;
        this.telefone = telefone;
        this.caldata = caldata;
    }

    public String getCodcliente() {
        return codcliente;
    }

    public void setCodcliente(String codcliente) {
        this.codcliente = codcliente;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Date getCaldata() {
        return caldata;
    }

    public void setCaldata(Date caldata) {
        this.caldata = caldata;
    }
       
        
}
