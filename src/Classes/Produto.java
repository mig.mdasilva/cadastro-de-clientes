package Classes;

public class Produto {
    
    private String codproduto;
    private String descricao;
    private int preco;
    private String obs;
    String produto;

    public Produto(String codproduto, String descricao, int preco, String obs, String produto) {
        this.codproduto = codproduto;
        this.descricao = descricao;
        this.preco = preco;
        this.obs = obs;
        this.produto = produto;
    }




    public String getCodproduto() {
        return codproduto;
    }

    public void setCodproduto(String codproduto) {
        this.codproduto = codproduto;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getProduto() {
       
        return produto;
    }

    public void setProduto(String produto) {
        this.produto = produto;
    }


    public int getPreco() {
        return preco;
    }

    public void setPreco(int preco) {
        this.preco = preco;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

   
}
