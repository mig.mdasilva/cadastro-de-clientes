
package Formularios;

import Classes.Dados;
import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;


public class frmClientes extends javax.swing.JInternalFrame {

    private Dados clsdados;
    private int Cliatual=0;
    private boolean cmdNovo=false;
    private DefaultTableModel Usertable;
    
    public void setDados(Dados clsdados){
        this.clsdados = clsdados;
    }
    public frmClientes() {
        initComponents();
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtcodcliente = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtnome = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtsobrenome = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        cmddeletar = new javax.swing.JButton();
        cmdnovo1 = new javax.swing.JButton();
        cmdsalvar = new javax.swing.JButton();
        cmdpesquisar = new javax.swing.JButton();
        cmdalterar = new javax.swing.JButton();
        cmdultimo = new javax.swing.JButton();
        cmdanterior = new javax.swing.JButton();
        cmdprimeiro = new javax.swing.JButton();
        cmdcancelar = new javax.swing.JButton();
        cmdproximo = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        Mtable = new javax.swing.JTable();
        jLabel7 = new javax.swing.JLabel();
        txtendereco = new javax.swing.JTextField();
        txttelefone = new javax.swing.JTextField();
        caldata = new com.toedter.calendar.JCalendar();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(jTable1);

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Cadastro De Clientes");
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameOpened(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 0));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 3, 24)); // NOI18N
        jLabel1.setText("Cod Cliente: ");

        jLabel2.setFont(new java.awt.Font("Times New Roman", 3, 24)); // NOI18N
        jLabel2.setText("Nome: ");

        jLabel3.setFont(new java.awt.Font("Times New Roman", 3, 24)); // NOI18N
        jLabel3.setText("Sobrenome: ");

        jLabel4.setFont(new java.awt.Font("Times New Roman", 3, 24)); // NOI18N
        jLabel4.setText("Endereço:");

        jLabel5.setFont(new java.awt.Font("Times New Roman", 3, 24)); // NOI18N
        jLabel5.setText("Telefone:");

        cmddeletar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/delete_4219.png"))); // NOI18N
        cmddeletar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmddeletarActionPerformed(evt);
            }
        });

        cmdnovo1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/NovoUsuario.png"))); // NOI18N
        cmdnovo1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdnovo1ActionPerformed(evt);
            }
        });

        cmdsalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/Salvar.png"))); // NOI18N
        cmdsalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdsalvarActionPerformed(evt);
            }
        });

        cmdpesquisar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/Pesquisar.png"))); // NOI18N
        cmdpesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdpesquisarActionPerformed(evt);
            }
        });

        cmdalterar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/Editar.png"))); // NOI18N
        cmdalterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdalterarActionPerformed(evt);
            }
        });

        cmdultimo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/Ultimo.png"))); // NOI18N
        cmdultimo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdultimoActionPerformed(evt);
            }
        });

        cmdanterior.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/Anterior.png"))); // NOI18N
        cmdanterior.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdanteriorActionPerformed(evt);
            }
        });

        cmdprimeiro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/Primeiro.png"))); // NOI18N
        cmdprimeiro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdprimeiroActionPerformed(evt);
            }
        });

        cmdcancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/Cancelar.png"))); // NOI18N
        cmdcancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdcancelarActionPerformed(evt);
            }
        });

        cmdproximo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/Proximo.png"))); // NOI18N
        cmdproximo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdproximoActionPerformed(evt);
            }
        });

        Mtable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "1", "2", "3", "3"
            }
        ));
        jScrollPane1.setViewportView(Mtable);

        jLabel7.setText("Data:");

        txtendereco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtenderecoActionPerformed(evt);
            }
        });

        txttelefone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txttelefoneActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(cmdprimeiro)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cmdanterior)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cmdproximo)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cmdultimo)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cmdcancelar))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtsobrenome, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel5))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txttelefone, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtendereco, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(jLabel1)
                                                .addGap(0, 0, Short.MAX_VALUE))
                                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtnome)
                                            .addComponent(txtcodcliente))))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(cmdalterar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cmdpesquisar)
                                .addGap(18, 18, 18)
                                .addComponent(cmdsalvar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cmdnovo1)
                                .addGap(18, 18, 18)
                                .addComponent(cmddeletar)
                                .addGap(15, 15, 15))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addGap(18, 18, 18)
                                .addComponent(caldata, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(txtcodcliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(txtnome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(txtsobrenome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(15, 15, 15)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(txtendereco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(txttelefone, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(caldata, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(cmdnovo1)
                    .addComponent(cmddeletar)
                    .addComponent(cmdalterar)
                    .addComponent(cmdultimo)
                    .addComponent(cmdcancelar)
                    .addComponent(cmdproximo)
                    .addComponent(cmdanterior)
                    .addComponent(cmdprimeiro)
                    .addComponent(cmdpesquisar)
                    .addComponent(cmdsalvar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 357, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(59, 59, 59))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    

    private void formInternalFrameOpened(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameOpened
    txtcodcliente.setText(clsdados.getClientes()[Cliatual].getCodusuario());
    txtnome.setText(clsdados.getClientes()[Cliatual].getNome());
    txtsobrenome.setText(clsdados.getClientes()[Cliatual].getSobrenome());
    txtsenha.setText(clsdados.getClientes()[Cliatual].getSenha());
    txtconfsenha.setText(clsdados.getClientes()[Cliatual].getSenha());
    
    }//GEN-LAST:event_formInternalFrameOpened

    private void cmdproximoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdproximoActionPerformed
        // TODO add your handling code here:
        Cliatual = clsdados.NUsuarios();
        if (Cliatual == clsdados.NUsuarios()){
            Cliatual=0;
        }
    }//GEN-LAST:event_cmdproximoActionPerformed

    private void cmdcancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdcancelarActionPerformed
        // TODO add your handling code here:
        cmdsalvar.setEnabled(false);
   

        txtcodcliente.setEnabled(false);
        txtnome.setEnabled(false);
        txtsobrenome.setEnabled(false);
     

        // cmdNovo=true;
    }//GEN-LAST:event_cmdcancelarActionPerformed

    private void cmdprimeiroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdprimeiroActionPerformed
        // TODO add your handling code here:
        Cliatual = 0;
    }//GEN-LAST:event_cmdprimeiroActionPerformed

    private void cmdanteriorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdanteriorActionPerformed
        // TODO add your handling code here:
        Cliatual --;
        if(Cliatual == -1){
            Cliatual = clsdados.NUsuarios()-1;
        }
        visualizarCadastros();
    }//GEN-LAST:event_cmdanteriorActionPerformed

    private void cmdultimoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdultimoActionPerformed
        //Esse é o codigo do botão de ultimo
        Cliatual = clsdados.NUsuarios();
        visualizarCadastros();
    }//GEN-LAST:event_cmdultimoActionPerformed

    private void cmdalterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdalterarActionPerformed
        txtnome.setEnabled(true);
        txtsobrenome.setEnabled(true);
        txtsenha.setEnabled(true);
        txtconfsenha.setEnabled(true);

        cmdNovofalse;
        txtnome.requestFocusInWindow();
    }//GEN-LAST:event_cmdalterarActionPerformed

    private void cmdpesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdpesquisarActionPerformed
        //este codigo é para botao pesquisar,  (se for ter)

        String usuario = JOptionPane.showInputDialog("Favor digitar o codigo do usuario para realizar sua pesquisa");
        if(usuario.equals("")){
            return;
        }
        int posL = clsdados.LinhaUsuario(usuario);
        if (posL == -1){
            JoptionPane.showMessageDialog(rootPane, " Nao foi possivel localiza este cadastro no nosso banco de dados")
            return;
        }
        Cliatual=posL;
        visualizarCadastros();
    }//GEN-LAST:event_cmdpesquisarActionPerformed

    private void cmdsalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdsalvarActionPerformed
        //validando os campo no formulario de usuario

        if (txtcodcliente.getText().equals("")) {
            JOptionPane.showMessageDialog(rootPane, "O Campo codigo do usuario não pode ficar em branco!");
            txtcodcliente.requestFocusInWindow();
            return;

        }
        if (txtcodcliente.getText().equals("")) {
            JOptionPane.showMessageDialog(rootPane, "O Campo codigo do usuario não pode ficar em branco!");
            txtcodcliente.requestFocusInWindow();
            return;

        }
        if (txtnome.getText().equals("")) {
            JOptionPane.showMessageDialog(rootPane, "O Campo nome do usuario não pode ficar em branco!");
            txtcodcliente.requestFocusInWindow();
            return;

        }
        if (txtsobrenome.getText().equals("")) {
            JOptionPane.showMessageDialog(rootPane, "O Campo Sobrenome do usuario não pode ficar em branco!");
            txtcodcliente.requestFocusInWindow();
            return;

        }

        String SSenha= new String (txtsenha.getPassword());
        String comfirmarsenha= new String(txtcomfirmarsenha.getPassword());

        if (SSenha.equals("")) {
            JOptionPane.showMessageDialog(rootPane, "O Campo Senha do usuario não pode ficar em branco!");
            txtcodcliente.requestFocusInWindow();
            return;

        }
        if (comfirmarsenha.equals("")) {
            JOptionPane.showMessageDialog(rootPane, "O Campo confirmar a senha do usuario não pode ficar em branco!");
            txtcodcliente.requestFocusInWindow();
            return;

        }
        if (!SSenha.equals(comfirmarsenha)) {
            JOptionPane.showMessageDialog(rootPane, "O Campo de Confirmar a senha deve ser igual a senha!");
            txtcodcliente.requestFocusInWindow();
            return;

        }

        int poslinha= clsdados.LinhaUsuario(txtcodcliente.getText());
        if (cmdNovo) {
            if (poslinha != -1) {
                JOptionPane.showMessageDialog(rootPane, "Este cadastro ja existe");
                txtcodcliente.requestFocusInWindow();
                return;

            }

        }

        else{
            if (poslinha != -1) {
                JOptionPane.showMessageDialog(rootPane, "Este cadastro não existe ainda");
                txtcodcliente.requestFocusInWindow();
                return;

            }

            Usuario MUsuario = new Usuario(txtcodcliente.getText(), txtnome.getText(), txtsobrenome.getText(), SSenha);

            String msg;

            if (cmdNovo) {
                msg= clsdados.EditarUsuario(MUsuario, poslinha);

            }else{
                msg= clsdados.EditarUsuario(MUsuario,poslinha);
            }

            JOptionPane.showMessageDialog(rootPane, msg);

            CarregarTable();
            //Esse é o codigo do botão de salvar

            cmdsalvar.setEnabled(false);
            cmdultimo.setEnabled(false);

            txtcodcliente.setEnabled(false);
            txtnome.setEnabled(false);
            txtsobrenome.setEnabled(false);
            txtsenha.setEnabled(false);
            txtcomfirmarsenha.setEnabled(false);
    }//GEN-LAST:event_cmdsalvarActionPerformed

    private void cmdnovo1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdnovo1ActionPerformed
        // TODO add your handling code here:
        
        
        cmdanterior.setEnabled(true);
        cmdproximo.setEnabled(true);
        cmddeletar.setEnabled(true);
        cmdalterar.setEnabled(false);
        cmdcancelar.setEnabled(false);
        cmdsalvar.setEnabled(true);
        cmdpesquisar.setEnabled(true);

        txtcodcliente.setEnabled(true);
        txtnome.setEnabled(true);
        txtsobrenome.setEnabled(true);
        txtcodcliente.setEnabled(true);
        txttelefone.setEnabled(true);
        txtendereco.setEnabled(true);
        caldata.setEnabled(true);
        
        
        
        txtcodcliente.setText("");
        txtnome.setText("");
        txtsobrenome.setText("");
        txtcodcliente.setText("");
        txttelefone.setText("");
        txtendereco.setText("");
        caldata.setDate(new Date ());
      
   

        txtcodcliente.setText("");
       
    

        txtcodcliente.requestFocusInWindow();

        CarregarTable();
        
        int Del = JOptionPane.showConfirmDialog(rootPane, "Tem certeza que deseja deletar este cadastro?");
        if (Del !=0) {
            return;

        }
        String msg;
        msg = clsdados.DeletarCliente(Cliatual);
        JOptionPane.showMessageDialog(rootPane, ui);
        Cliatual = 0;
        visualizarCadastros();
        CarregarTable();

    }//GEN-LAST:event_cmdnovo1ActionPerformed

    private void cmddeletarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmddeletarActionPerformed
        // Este é o codigo do botão deletar

    }//GEN-LAST:event_cmddeletarActionPerformed

    private void txtenderecoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtenderecoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtenderecoActionPerformed

    private void txttelefoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txttelefoneActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txttelefoneActionPerformed
private void visualizarCadastros(){
        txtcodcliente.setText(clsdados.getClientes()[Cliatual].getCodusuario());
        txtnome.setText(clsdados.getClientes()[Cliatual].getNome());
        txtsobrenome.setText(clsdados.getClientes()[Cliatual].getSobrenome());
        
}
private void CarregarTable(){
 String titulocabecalho[]={"Codido do usuario ","Nome","Sobre Nome"};
 String RegCadastro[]= new String[4];
 Usertable=new DefaultTableModel(null, titulocabecalho);
    for (int i = 0; i < clsdados.NUsuarios(); i++) {
        RegCadastro[0]=clsdados getClientes()[i].getCodusuario();
        RegCadastro[1]=clsdados getClientes()[i].getNome();
        RegCadastro[2]=clsdados getClientes()[i].getSobrenome();
        

        Usertable.addRow(RegCadastro);
    }
    Mtable.setModel(usertable);
}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable Mtable;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private com.toedter.calendar.JCalendar caldata;
    private javax.swing.JButton cmdalterar;
    private javax.swing.JButton cmdanterior;
    private javax.swing.JButton cmdcancelar;
    private javax.swing.JButton cmddeletar;
    private javax.swing.JButton cmdnovo1;
    private javax.swing.JButton cmdpesquisar;
    private javax.swing.JButton cmdprimeiro;
    private javax.swing.JButton cmdproximo;
    private javax.swing.JButton cmdsalvar;
    private javax.swing.JButton cmdultimo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField txtcodcliente;
    private javax.swing.JTextField txtendereco;
    private javax.swing.JTextField txtnome;
    private javax.swing.JTextField txtsobrenome;
    private javax.swing.JTextField txttelefone;
    // End of variables declaration//GEN-END:variables
}
